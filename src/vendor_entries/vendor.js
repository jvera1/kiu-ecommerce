// All imports in this file will be compiled into vendors.js file.
//
// Note: ES6 support for these imports is not supported in base build

module.exports = [
  './node_modules/jquery/dist/jquery.js',
  './node_modules/bootstrap/dist/js/bootstrap.min.js',
  './node_modules/select2/dist/js/select2.min.js',
  './node_modules/air-datepicker/dist/js/datepicker.min.js',
  './node_modules/air-datepicker/dist/js/i18n/datepicker.es.js',
];
