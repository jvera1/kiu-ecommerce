// You can write a call and import your functions in this file.
//
// This file will be compiled into app.js and will not be minified.
// Feel free with using ES6 here.


(($) => {
  // When DOM is ready
  

  $(() => {
	$( ".grid-7x7__card" ).click(function() {
		if ($(".--active")[0]){
			// Do something if class exist
			$( ".grid-7x7__card" ).removeClass( "--active" ),
			$( this ).toggleClass( "--active" ),
			$( "body" ).addClass( "--summary-active --fare-selected" );

			//togle Details
			if( $(window).width() > 992 ) {
				$(this).closest(".grid-7x7__flight").find(".collapse").collapse('toggle');
			}
		} else {
			// Do something if class does not exist
			$( this ).toggleClass( "--active" ),
			$( "body" ).addClass( "--summary-active  --fare-selected" );		    
			if( $(window).width() > 992 ) {
				$(this).closest(".grid-7x7__flight").find(".collapse").collapse('toggle');
			}
			
		}
	 
	});

	// $(".arrow").click(function() {
	//     $(this).closest(".container").find(".box").slideToggle();
	// });

	// BUTTON CONTINUE
	$("#btn-continue").click(function() {
		$( "body" ).toggleClass( "--summary-active" );
	});
	

	$("#btn-summary-lg").click(function() {
		$( "body" ).toggleClass( "--summary-active" );
	});

	$("#passenger-class-btn").focus(function() {
		$( ".search-form__container" ).toggleClass( "--pc-active" );
		setTimeout(function(){
			$('#pc-adults').focus();
		},200);
	});

	$("#add-pc").click(function() {
		$( ".search-form__container" ).toggleClass( "--pc-active" );
	});
	$(".search-form__flight-type").click(function() {
		//$( "#inputReturnFlight" ).toggleClass( "d-none" );
		$( "#inputReturnDate" ).toggleClass( "d-none" );
	});

	// MAIN SEARCH FORM
	 $('#search-form__select-currency').select2();

	// MAIN SEARCH FORM
	 $('#loc-1-btn').select2();
	 $('#loc-2-btn').select2();

	 

	$('#loc-1-btn').datepicker();
	// Access instance of plugin
	$('#loc-1-btn').data('datepicker');

	$('.my-datepicker').datepicker({
	    language: 'es'
	})
	// $('#changeDate').datepicker({
	//     startDate: '23/4/2018'
	// })


  });
})(jQuery);

